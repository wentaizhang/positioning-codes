%  Compute 4 maps of ionospheric error corrections
%   using the following ionospheric parameters taken from a Navigational file in RINEX format:
line1 = 'GPSA   7.4506D-09  1.4901D-08 -5.9605D-08 -1.1921D-07       IONOSPHERIC CORR';
line2 = 'GPSB   9.2160D+04  1.3107D+05 -6.5536D+04 -5.2429D+05       IONOSPHERIC CORR';
ionoparams = [cell2mat(textscan(line1, '%*s %f %f %f %f %*s')) ...
              cell2mat(textscan(line2, '%*s %f %f %f %f %*s'))];
az=0;   % At 90 degrees the azimut is not relevant for the computation of the ionospheric map, so set az = 0
t=0:6*3600:18*3600; % time steps for the computation (in seconds), at 0:00, 6:00, 12:00, 18:00 
elevation = 90; % Set the elevation of the hypothetic satellite to 90 degrees
% Define the step of the grid map
latitude = -80:0.5: 80; % in degree
longitude = -180:0.5:180; % in degree
yn=length(latitude);
xn=length(longitude);
tn=length(t);
corrMap=zeros(yn,xn,tn); % Preallocation of the corrMap as a vector of matrices. Each matrix corresponds to a world map of ionospheric error
for i=1:yn
    for j=1:xn
        for k=1:tn
            corrMap(i,j,k)=iono_error_correction(latitude(i),longitude(j),az,elevation,t(k),ionoparams);
        end
    end
end
% Plot the computed maps
% Vectors phi and lambda
% Use the meshgrid function to produce the coordinates of a rectangular grid
[lambda,phi]=meshgrid(longitude,latitude);
for a = 1 : tn   
    figure
    axesm ('eqdcylin', 'Frame', 'on', 'Grid', 'on', 'LabelUnits', 'degrees', 'MeridianLabel', 'on', 'ParallelLabel', 'on', 'MLabelParallel', 'south');
    geoshow('landareas.shp', 'FaceColor', 'none');
    hold on
    geoshow(phi,lambda,corrMap(:,:,a),'FaceAlpha', 0.9,'DisplayType','texturemap');
    colormap(jet(1024));
    geoshow(phi,lambda,corrMap(:,:,a),'DisplayType','contour','LineColor','black');
    c=colorbar('southoutside');
    hold off
    xlabel('Geographic longitude (degree)');
    ylabel('Geographic latitude (degree)');
    c.Label.String = 'TEC (TECU)';
    title(['Global Ionospheric Error Correction Map at ' num2str(mod(t(a)/3600,24),'%02d:00')]);
end

%  Compute a polar map of ionospheric error corrections for an observer located in Milan
time=(0:12*3600:12*3600); % at 0:00, 12:00 (GMT) time steps for the computation (in seconds)
elevation=0:0.5:90; % elevation = from 0 to 90 (step 0.5)
azimuth=-180:0.5:180; % azimuth = from -180 to 180 (step 0.5);
Lat=45.4642700; % Coordinates of Milan (http://dateandtime.info/it/citycoordinates.php?id=3173435)
Lon=9.1895100;
ne=length(elevation);
na=length(azimuth);
ntime=length(time);
% Preallocation of the corrMap. Each matrix corresponds to a polar map of ionospheric error centred on Milan
corr_map_Mi=zeros(ne,na,ntime);
% Fill the corrMap vector using the iono_error_correction function
for r=1:ntime
    for p=1:ne
        for q=1:na
            corr_map_Mi(p,q,r)=iono_error_correction(Lat,Lon,azimuth(q),elevation(p),time(r),ionoparams);
        end
    end
end
[azi,ele]=meshgrid(azimuth,elevation);  % Use the meshgrid function to produce the coordinates of a rectangular grid

% Plot 2 figures using the following code
for t = 1 : ntime    
    figure
    axesm('eqaazim','MapLatLimit',[0 90])
    axis off
    framem on
    gridm on
    mlabel on
    plabel on;
    setm(gca,'MLabelParallel',0)
    geoshow(ele, azi +270, corr_map_Mi(:,:,t), 'DisplayType', 'texturemap');
    colormap(jet(1024));
    c=colorbar;
    c.Label.String = 'TEC (TECU)';
    title(['Ionospheric Error Correction Map at ' num2str(mod(time(t)/3600,24),'%02d:00') ' for an Observer Located in Milan']);
    hold off
end

