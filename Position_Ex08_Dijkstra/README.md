Results:

Time to reach the final node: 17
Kyoto <- Instanbul <- Agno <- Bergamo <- Saronno

Minimal path to reach all the visited nodes:
 Linate reached in 1, from:  <- Saronno
 Monza reached in 4, from:  <- Linate <- Saronno
 Bergamo reached in 8, from:  <- Saronno
 Agno reached in 11, from:  <- Bergamo <- Saronno
 Instanbul reached in 13, from:  <- Agno <- Bergamo <- Saronno
 Kyoto reached in 17, from:  <- Instanbul <- Agno <- Bergamo <- Saronno