% Ex. 08 Dijkstra
% Data:1)a set of nodes 2)a set of arcs
% An arc is defined by a couple of nodes (A, B) and a cost (e.g. time to
% reach the ending node); the arc can be traveled from A to B.
% Problem: 
% Try to find the fastest way to reach Kyoto from Saronno, using the provided nodes and arcs.
% ------------------
% Dijkstra algorithm
% -------------------
% 1) set the starting node and add it to a list containing the visited 
%    nodes (initialization). We suggest to use at least the following
%    variables, to be filled during the processing:
%    - 'ttn': array containing the travel time to reach each node from the starting node
%    - 'prev_node_id': array to store the predecessor of each node in the minimal path
%    - 'visited_node_id': array of visited nodes
%    - 'node_id_to_visit': array of nodes to visit
% 2) find all the outgoing arcs from the current node
%    - update the array 'node_id_to_visit'
%    - update 'ttn'
%    - update 'prev_node_id'
%
% 3) find the closer node in the list of nodes to visit and set it as the
%    current node (search the one with minimum ttn)
%    - add it to the list of visited nodes and
%    - remove it from the list of node to visit
% 
% 4) if the current node is the destination exit otherwise restart from 
%    point 2 (termination check) 
% 
% 5) display the minimal path.
%--------------------------------------------------------------------------
    % Id of the graph nodes
    node_city = {'Monza', 'Linate', 'Saronno', 'Agno', 'Bergamo', 'Instanbul', 'Kyoto'};
    node_indexes = {1, 2, 3, 4, 5, 6, 7};
    % Arc connecting two nodes
    % arc(:, 1) -> begin node
    % arc(:, 2) -> end node
    % arc(:, 3) -> weight of the arc
arc = [1 2   3; ...
       2 1   3; ...
       1 4  10; ...
       4 1  10; ...
       2 3   1; ...
       3 2   1; ...
       3 4  13; ...
       4 3  13; ...
       3 5   8; ...
       5 3   8; ...
       4 5   3; ...
       5 4   3; ...
       5 6   6; ...
       6 5   6; ...
       6 4   2; ...
       4 6   2; ...
       6 7   4; ...
       7 6   4; ...
       2 7  18; ...
       7 2  18];
    first_node_id = 3;            % Departure node
    % first_node_id = find(strcmp(node_city,'Saronno'));       % another way to find it from the vector of node_city
    final_node_id = 7;            % Arrival node
    % final_node_id = find(strcmp(node_city,'Kyoto'));         % another way to find it from the vector of node_city
tic;
ttn=inf(size(node_indexes)); % travel time to reach each node from the starting node
ttn(first_node_id)=0; % set to zero the time to reach the first node 
prev_node_id=nan(size(node_indexes)); % predecessor of each node in the minimal path
prev_node_id(first_node_id)=first_node_id;% set the prev_node_id of the first node to its id, because the first node is reachable only from itself
visited_node_id=[]; % array of visited nodes
% add to node_id_to_visit the id of the first node
node_id_to_visit=first_node_id;
while not(isempty(node_id_to_visit)) && not(ismember(final_node_id,visited_node_id))%% While I still have nodes to visit 
    [min_value, min_value_id] = min(ttn(node_id_to_visit));% find closer node searching the minimum in ttn vector
    % select inside node_id_to_visit the current node (curr_node_id) using the min_value_id
    curr_node_id=node_id_to_visit(min_value_id);
    %node_id_to_visit(curr_node_id)=[];
    node_id_to_visit=setdiff(node_id_to_visit,curr_node_id); % remove the current node from node_id_to_visit
    visited_node_id=[visited_node_id,curr_node_id]; % add the current node to visited_node_id
    if not(ismember(final_node_id,visited_node_id))    % IF the final node has not been reached
        outgoing_arcs_id=find(arc(:,1)==curr_node_id);  % check for reachable nodes from current node
        [reachable_set, ok_id] = setdiff(arc(outgoing_arcs_id, 2), visited_node_id, 'stable');
        outgoing_arcs_id=outgoing_arcs_id(ok_id); % remove from outgoing arcs those that are ending in visited nodes
        for i=1:length(reachable_set) % FOR each reachable node
            id_node = reachable_set(i); % create variable id_node equal to its id 
            if ttn(id_node)> (ttn(curr_node_id) + arc(outgoing_arcs_id(i),3))% check if the node reachable_set(i) can be reached in a faster way
                ttn(id_node)=ttn(curr_node_id) + arc(outgoing_arcs_id(i),3); % update ttn(id_node)
                prev_node_id(id_node)=curr_node_id; % update prev_node_id(id_node) with the id of the current node
                node_id_to_visit=[node_id_to_visit;id_node]; % update node_id_to_visit with id_node
            end
        end         
    end     
end
% Display the minimal path to reach the final node in term of ttn
fprintf('Time to reach the final node: %d\n',ttn(end))


% run this commands to display the whole path from Saronno to Kyoto
    cur_id = final_node_id;
    cur_node = node_city{cur_id};
    prev_node = node_city{prev_node_id(cur_id)};
    % use while cycle to display the full path
    % NOTE. the while condition is 'not(strcmp(cur_node, prev_node))'
    fprintf('Kyoto')
    while not(strcmp(cur_node, prev_node))
        fprintf(' <- %s', prev_node);
        cur_node = prev_node;
        cur_id = prev_node_id(cur_id);
        prev_node = node_city{prev_node_id(cur_id)};
    end
    fprintf('\n');
    % Try to run this commented commands to display the minimal path to
    % each single node (check the name of the variables)
    
fprintf('\nMinimal path to reach all the visited nodes:\n');
% Plot results (for all the visited nodes)
for i = 1 : length(visited_node_id)
    cur_id = visited_node_id(i);
    if (cur_id ~= first_node_id)
        cur_node = node_city{visited_node_id(i)};
        fprintf(' %s reached in %d, from: ', cur_node, ttn(cur_id));
        prev_node = node_city{prev_node_id(cur_id)};
        while not(strcmp(cur_node, prev_node))
            fprintf(' <- %s', prev_node);
            cur_node = prev_node;
            cur_id = prev_node_id(cur_id);
            prev_node = node_city{prev_node_id(cur_id)};
        end
        fprintf('\n');
    end
end
