% Initial position [m]
x0=4398306.279;
y0=704149.855;
z0=4550154.679;
xyz0=[x0,y0,z0];

% Accuracy of position
errx0=1/1000;
erry0=0.5/1000;
errz0=1.2/1000;
sigma_xyz0=[errx0,erry0,errz0];

% Velocity of the points movement
vx=-0.0145; % m/year
vy=0.0179; % m/year
vz=0.0112; % m/year
vxyz=[vx,vy,vz];

% Accuracy of velocity
errvx=0.2/1000;
errvy=0.1/1000;
errvz=0.15/1000;
sigma_vxyz=[errvx,errvy,errvz]; % Accuracy of velocity


t0=2005.00; % Starting year
td=2011.75; % Year of displacement
te=2018.00; % End year

% Change in position after displacement
dx=-1/100;
dy=-1/1000;
dz=-1/100;
dxyz=[dx,dy,dz];

% Accuracy of change of position
errdx=1/1000;
errdy=0.5/1000;
errdz=1.2/1000;
sigma_delta_xyz=[errdx,errdy,errdz];

% speed and accuracy of displacement
vdx=0;
vdy=0;
vdz=0;
vdxyz=[vdx,vdy,vdz];
errvdx=0;
errvdy=0;
errvdz=0;
sigma_vdxyz=[errvdx,errvdy,errvdz];


sigma2_xyz0=sigma_xyz0.^2;         % Variance for position
sigma2_vxyz=sigma_vxyz.^2;         % Variance for velocity
sigma2_dxyz=sigma_delta_xyz.^2;    % Variance for accuracy
sigma2_vdxyz=sigma_vdxyz.^2;       % Variance for displacement speed

dt=52*13;
t=t0:(te-t0)/dt:te;         % Create Time Vector
xyzs=zeros(length(t),3);    % Position Vector
sdevxyzs=zeros(length(t),3); % Variance Vector
for i=(1:length(t))         % Cycle for Each t
    if t(i)<td
        xyzs(i,:)=xyz0+vxyz*(t(i)-t(1));              % Calculate x Coordinate for each time
        sdevxyzs(i,:)=sqrt(sigma2_xyz0+sigma2_vxyz*(t(i)-t(1))^2);     % Calculate Standard Deviation for each time
    else
        xyzs(i,:)=xyz0+dxyz+vxyz*(t(i)-t(1))+vdxyz*(t(i)-td);                     % Calculate x Coordinate for each time
        sdevxyzs(i,:)=sqrt(sigma2_xyz0+sigma2_dxyz+sigma2_vxyz*(t(i)-t(1))^2+sigma2_vdxyz*(t(i)-td)^2);      % Calculate Standard Deviation for each time
    end
end
subplot(2,3,1);
plot(t,xyzs(:,1)-mean(xyzs(:,1)))       % Plot x Coordinate with Respect to the Mean Value
xlabel('Time (year)')
ylabel('Estimated Position in X Direction (m)')
subplot(2,3,2);
plot(t,xyzs(:,2)-mean(xyzs(:,2)))       % Plot y Coordinate with Respect to the Mean Value
xlabel('Time (year)')
ylabel('Estimated Position in Y Direction (m)')
subplot(2,3,3);
plot(t,xyzs(:,3)-mean(xyzs(:,3)))       % Plot z Coordinate with Respect to the Mean Value
xlabel('Time (year)')
ylabel('Estimated Position in Z Direction (m)')
subplot(2,3,4);                         % Plot Standard Deviation in x direction
plot(t,sdevxyzs(:,1))
xlabel('Time (year)')
ylabel('Standard Deviation in X Direction (m)')
subplot(2,3,5);                         % Plot Standard Deviation in y direction
plot(t,sdevxyzs(:,2))
xlabel('Time (year)')
ylabel('Standard Deviation in Y Direction (m)')
subplot(2,3,6);                         % Plot Standard Deviation in z direction
plot(t,sdevxyzs(:,3))
xlabel('Time (year)')
ylabel('Standard Deviation in Z Direction (m)')





