p1x1=65.23;                 % x coordinates of P1 in RF1
p1y1=40.97;                 % y coordinates of P1 in RF1
p1x2=97.1259;               % x coordinates of P1 in RF2
p1y2=21.2161;               % y coordinates of P1 in RF2
p2x1=25.65;                 % x coordinates of P2 in RF1
p2y1=-50.04;                % y coordinates of P2 in RF1
p2x2=17.3435;               % x coordinates of P2 in RF2
p2y2=-37.8110;              % y coordinates of P2 in RF2
y=[p1x2;p1y2;p2x2;p2y2];
A=[1,0,p1x1,p1y1;
   0,1,p1y1,-p1x1;
   1,0,p2x1,p2y1;
   0,1,p2y1,-p2x1];
x=A\y;
tx=x(1,1);
ty=x(2,1);
scale=sqrt(x(3,1)^2+x(4,1)^2);
rotatedegree=atand(x(4,1)/x(3,1));
fprintf('tx [m] = %8.4f\n',tx);
fprintf('ty [m] = %8.4f\n',ty);
fprintf('s = %11.10f\n',scale);
fprintf('theta [%c] = %9.7f\n',176,rotatedegree);