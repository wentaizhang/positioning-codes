% Como Brunate baseline definition
como_brun_DX = [-1040.168, -72.970, 1631.398]; % m
C_DX = [2,0.5,0.5;0.5,1.0,0.5;0.5,0.5,2.0] * 1e-6;
% -------------------------------------------------------------------------
% A) Fix COMO to its ITRF known coordinates at 2nd of October 2017.
% -------------------------------------------------------------------------
%
% 1. Convert date (2/10/2017) in fraction of year 
%    t = year + (day of the year) / 365
%
%    NB. for day of the year (doy) look at http://www.gnsscalendar.com
% 2. From EUREF website  (http://www.epncb.oma.be)
%    select:                Network & Data -> Station List
%    search for:            COMO station
%    select:                Position, velocity & Time series --> COMO00ITA

%    read:
%    - coordinates of COMO station and their precision
%    - velocities of COMO station and their precision
%    - reference epoch t0
%
% 	 NB. data from first row of table 'IGb08' 
t0 = 2005;
t = 2017 + 275 / 365;
como_posi0=[4398306.278, 704149.859, 4550154.676];
como_velo0=[-0.0145, 0.0179, 0.0112];
% 3. Create covariance diagonal matrices of COMO positions and velocities 
%    using the squared values of precisions read from EUREF website
%
%    NB. The precisions indicated in the tables below are formal 1 sigma errors 
%        from the normal equation stacking. Due to the rounding formal errors 
%        indicated by 0.000 m (0.0000 m/y) must be read as being smaller 
%        than 0.0005 m (0.00005 m/y) or 0.5 mm (0.05mm/y). 
%
%        SO USE FOR PRECISIONS: 
%        0.0005 m for positions and 0.00005 m/y for velocities
posi_err=0.0005;
velo_err=0.00005;
cova_posi0=posi_err^2*eye(3);
cova_velo0=velo_err^2*eye(3);
% -------------------------------------------------------------------------
% B) Compute cartesian geocentric coordinates of COMO at 2017/10/2.
% -------------------------------------------------------------------------
%
% 4. Apply the following formulas to compute COMO cartesian geocentric 
%    position at epoch t and its corresponding covariance matrix
%
%    X = X0 + V0 * (t - t0);
%    CX = CX0 + (t - t0)^2 * CVX0;
%
%    Display at screen the results using 'fprintf' (for more information 'help fprintf')
posi=como_posi0+como_velo0*(t-t0);
cova_posi=cova_posi0+(t-t0)^2*cova_velo0;
fprintf( '\nComo ITRF coordinates: \n %f %f %f\n ', posi(1), posi(2), posi(3));
fprintf( '\nComo Covariance matrix: \n %d %d %d\n %d %d %d\n %d %d %d\n', cova_posi);
% -------------------------------------------------------------------------
% C) Compute the BRUN coordinates in ITRF at 2017/10/2.
% -------------------------------------------------------------------------
%
% 5. Apply the following formulas to derive BRUN cartesian geocentric
%    coordinates and its corresponding covariance matrix
%    
%    X_B = X + DX
%    CX_B = CX + CDX
%
%    Display at screen the results
posi_B=posi+como_brun_DX;
cova_posi_B=cova_posi+C_DX;
fprintf( '\nBrun ITRF coordinates: \n %f %f %f\n ', posi_B(1), posi_B(2), posi_B(3));
fprintf( '\nBrun Covariance matrix: \n %d %d %d\n %d %d %d\n %d %d %d\n', cova_posi_B);
% -------------------------------------------------------------------------
% D) Compute the BRUN coordinates in local cartesian.
% -------------------------------------------------------------------------
%
% 6. Compute geodetic coordinates of COMO (lambda, phi, h)
%
%    Display at screen the results

% defining geoid (GRS80):
a = 6378137;
f = 1/298.257222101;
% 6.1 Compute:
%     - b semi-minor axis (b) 
%     - squared first eccentricity (e2) 
%     - squared second eccentricity (eb2) 
%     - radius in XY-plane (r) 
%     - psi 
%     - longitude (lam) 
%     - latitude (phi)
%     - N
%     - h
b=a-a*f;
e2=(a^2-b^2)/a^2;
eb2=(a^2-b^2)/b^2;
r=sqrt(posi(1,1)^2+posi(1,2)^2);
psi=atan(posi(1,3)/(r*sqrt(1-e2)));
lam=atan(posi(1,2)/posi(1,1));
phi=atan((posi(1,3)+eb2*b*sin(psi)^3)/(r-e2*a*cos(psi)^3));
N=a/sqrt(1-e2*sin(phi)^2);
h=r/cos(phi)-N;
% 7. Compute local coordinates of BRUN and its corresponding covariance
%    matrix and display at screen the results.
%
%    NB. use the rotation matrix L
L=[-sin(lam),cos(lam),0;
    -sin(phi)*cos(lam),-sin(phi)*sin(lam),cos(phi);
    cos(phi)*cos(lam),cos(phi)*sin(lam),sin(phi)];
I=L*como_brun_DX';
cova2c_dx=L*C_DX*L';
fprintf( '\nBrun	Local	Cartesian	coordinates: \n%f  %f %f\n ', I(1), I(2), I(3));
fprintf( '\nCovariance of Brun	Local	Cartesian	coordinates \n%d  %d %d\n ', cova2c_dx(1,1), cova2c_dx(2,2), cova2c_dx(3,3));
% -------------------------------------------------------------------------
% E) Convert COMO geodetic coordinates from radiants to sexagesimal
% -------------------------------------------------------------------------
% 
%    Display at screen the results
%
%    NB. use 'floor' function (for more information 'help floor')
lamdeg=lam/pi*180;
lamd=floor(lamdeg);
lamm=floor(60*(lamdeg-lamd));
lams=60*(60*(lamdeg-lamd)-lamm);
phideg=phi/pi*180;
phid=floor(phideg);
phim=floor(60*(phideg-phid));
phis=60*(60*(phideg-phid)-phim);
fprintf ('\nComo	geodetic	coordinates(in sexagesimal):\nlambda= %f %f %f" \n', lamd, lamm, lams);
fprintf ('phi= %f %f? %f" \n', phid, phim, phis);
fprintf ('h:%f meters \n', h);
% -------------------------------------------------------------------------
% F) Transform BRUN coordinates from ITRF to ETRF coordinates
% -------------------------------------------------------------------------
%    use EUREF-EPN website tools at:
%    http://www.epncb.oma.be/_productsservices/coord_trans/index.php
%    input in ITRF2008 
%      x(m)           y(m)          z(m)           vx(m/y) vy(m/y) vz(m/y)
% BRUN 4397265.925075 704077.117286 4551786.216838 -0.0145 0.0179 0.0112
% 
%    output in ETRF2000
%      x(m)           y(m)          z(m)         vx(m/y)  vy(m/y)  vz(m/y)
% BRUN 4397266.20160  704076.86960 4551785.99450 -0.00053 -0.00061 -0.00040
