![](https://i.imgur.com/ppxDZm6.png)

## Problem

COMO PS belongs to EPN: its geocentric cartesian coordinates are published in the EPN website. The baseline from COMO to BRUN has been estimated at October, 2nd, 2017.
Compute BRUN geocentric cartesian coordinates in ITRF2008. Then, compute its local cartesian coordinates with respect to COMO. Finally, compute BRUN coordinates in ETRF2000. Compute also BRUN covariances in all the RFs. In covariance attribution to COMO coordinates, pay attention to the rounding standards of EPN.

## Wroking Flow

1. Fix COMO to its ITRF known coordinates at 2nd of October, 2017
2. Compute cartesian geocentric coordinates of COMO at 2017/10/2
3. Compute the BRUN coordinates in ITRF at 2017/10/2
4. Compute local coordinates of BRUN with respect to COMO
5. Convert COMO geodetic coordinates from radians to sexagesimals units
6. Transform BRUN coordinates from ITRF to ETRF coordinates, by using EUREF-EPN website tools: write just the results in the Excel sheet

Formulas of each step can be found in the Guidelines.pdf file.

## How to run
Open the Ex01_RF_GC2G.m in MATLAB and run it.