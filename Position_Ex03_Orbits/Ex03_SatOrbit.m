% For the interested reader: 
% http://www.navipedia.net/index.php/Clock_Modelling
% http://www.ajgeomatics.com/SatelliteOrbitsRev0828-2010.pdf
% http://www.navipedia.net/index.php/GPS_and_Galileo_Satellite_Coordinates_Computation
%set(0,'DefaultFigureWindowStyle','docked');
% Almanac of satellite SVN 63, PRN 01 (Block IIR) for 2017
dt0 = -7.661711424589E-05;                          % SV clock offset a0 
dt1 = -3.183231456205E-12;                          % SV clock drift a1 
dt2 =  0.000000000000E+00;                          % SV clock drift rate a02
sqrt_a = 5.153650835037E+03;                        % sqrt major semi-axis
a = (5.153650835037E+03)^2;                         % [m]
e = 3.841053112410E-03;                             % eccentricity
M0 = 1.295004883409E+00;                            % mean anomaly [rad]
Omega0 = -2.241692424630E-01;                       % right ascension of the ascending node [rad]
OmegaDot = -8.386063598924E-09;                     % rate of node's right ascension [rad/sec]
i0 = 9.634782624741E-01;                            % orbit inclination [rad]
iDot = -7.286017777600E-11;                         % rate of orbit inclination angle [rad/sec]
omega0 = 9.419793734505E-01;                        % perigee argument [rad]
omegaDot = 0.0;                                     % rate of perigeee argument [rad/sec]
GMe = 3.986005e14;                                  % Earth gravitational constant [m3/s2]
OmegaEdot = 7.2921151467 * 1E-5;                    % Earth angular velocity [rad/sec]
t0=0;
t_end=23*3600+59*60;
Dt=30;
t=(t0:Dt:t_end)';
N=length(t);
% B) Compute clock offsets and plot it
%    NOTE: The clock offsets are due to clock synchronism errors referring to GNSS (GPS, GLONASS, Galileo...) time scale.
offset=dt0+dt1*(t-t0)+dt2*(t-t0).^2;
figure
plot(t,offset)
xlabel('seconds in one day (00:00-23:59=86400 sec)');
ylabel('clock offsets[s]');
% C) Compute positions in ITRF (X, Y, Z)
% C.1) Compute mean motion
n=sqrt(GMe/a^3);
% C.2) Initialize to zero matrices of sat_coord_ORS and sat_coord_ITRF with
%      dimensions 3-by-n_epochs
sat_coord_ORS=zeros(N,3);
sat_coord_ITRF=zeros(N,3);
% C.3) For each epoch (use 'for' cycle) estimate:
M=zeros(N,1);
Eta=zeros(N,1);
pesi=zeros(N,1);
omega=zeros(N,1);
it=zeros(N,1);
Omega=zeros(N,1);
rt=zeros(N,1);
xt=zeros(N,1);
yt=zeros(N,1);
R3_Omega=zeros(3,3,N);
R1_i=zeros(3,3,N);
R3_omega=zeros(3,3,N);
 for i=1:N
     M(i)=M0+n*(t(i)-t0); % mean anomaly
     [Eta(i)]=ecc_anomaly(M(i), e); % eccentricity anomaly
     pesi(i)=atan2(sqrt(1-e^2)*sin(Eta(i)),cos(Eta(i))-e);  %   true anomaly
     omega(i)=omega0+omegaDot*(t(i)-t0);   % angle omega: rotation around z axis to move x from the perigee to the intersection of the orbital plane with the equatorial plane
     it(i)=i0+iDot*(t(i)-t0);  % angle i: rotation around x axis to move the orbital plane on the equatorial plane
     Omega(i)=Omega0+(OmegaDot-OmegaEdot)*(t(i)-t0);    % angle Omega: rotation around z axis to move the x axis from the intersection of the orbital plane with the equatorial plante towards the greenwich meridian
     Omega(i)=rem(Omega(i),2*pi);
     rt(i)=a*(1-e^2)/(1+e*cos(pesi(i)));    % coordinates with respect to the focus
     %rt(i)=a*(1-e*cos(Eta(i)));
     xt(i)=rt(i)*cos(pesi(i));      % satellite coordinates in the ORS (Orbit Reference System) 
     yt(i)=rt(i)*sin(pesi(i));
     sat_coord_ORS(i, :) =[rt(i),xt(i),yt(i)];% satellite coordinates in the ORS (Orbit Reference System)
     R3_Omega(:,:,i)=[cos(Omega(i)),-sin(Omega(i)),0;    % Rotation matrices 
         sin(Omega(i)),cos(Omega(i)),0;
         0,0,1];
     R1_i(:,:,i)=[1,0,0;                % NOTE: R3 stand for rotation around z-axis, R1 rotation around x-axis
         0,cos(it(i)),-sin(it(i));
         0,sin(it(i)),cos(it(i))];
     R3_omega(:,:,i)=[cos(omega(i)),-sin(omega(i)),0;
         sin(omega(i)),cos(omega(i)),0;
         0,0,1];
     sat_coord_ITRF(i,:)=(R3_Omega(:,:,i)*R1_i(:,:,i)*R3_omega(:,:,i)*[xt(i);yt(i);0])';  % satellite coordinates in the ITRF
 end
[Geo]=cart2geoW84(sat_coord_ITRF);% Convert ITRF satellite coordinates (X(t), Y(t), Z(t)) in geodetic coodinates (phi(t), la(t), h_ell(t))
% E) Plot groundtracks
figure
subplot(3,1,1:2);
% axesm --> define map axes and set map properties
ax = axesm ('eqdcylin', 'Frame', 'on', 'Grid', 'on', 'LabelUnits', 'degrees', 'MeridianLabel', 'on', 'ParallelLabel', 'on', 'MLabelParallel', 'south');
% geoshow --> display map latitude and longitude data 
% DISPLAYTYPE can be 'point', 'line', or 'polygon'; default is 'line'
geoshow('landareas.shp', 'FaceColor', 'black');
hold on
geoshow(rad2deg(Geo(:,1)),rad2deg(Geo(:,2)),'DisplayType','point','MarkerEdgeColor','green'); % change the unit from radius to degree
axis equal; axis tight;
subplot(3,1,3);
% plot the ellipsoidic height variations [km] around mean height
plot(t,(Geo(:,3)-mean(Geo(:,3)))/1000) % change the unit from meter to kilometer
xlabel('seconds in one day (00:00-23:59=86400 sec)');
ylabel('[km]');
title('ellipsoidic height variations [km] around mean height=20189.3244km');
% F) Print on a text file the results: Coordinates ORS - Coordinates ITRF - Coordinates phi, lambda, h ell
%    NOTE: use 'fopen', 'fprintf', 'fclose' functions 
fileID=fopen('coordinates.txt','w');
fprintf(fileID,'%s\r\n','* Epoch (s) || Coordinates ORS (xF,yF) || Coordinates ITRF (x,y,z) || Coordinates phi,lambda, h ell');
for i=1:N
    fprintf(fileID,'%5.3f || %10.8f %10.8f || %10.8f %10.8f %10.8f || %10.8f,%10.8f,%10.8f\r\n',[t(i),sat_coord_ORS(i,2:3),sat_coord_ITRF(i,:),Geo(i,:)]);
end
fclose(fileID);


