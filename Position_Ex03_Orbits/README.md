Almanac of satellite SVN 63, PRN 01 (Block IIR) for 2017 are given.

From 00.00 to 23.59 each 30 seconds:
- Compute clock offsets and plot it
- Compute satellite positions in ORS and ITRF (X, Y, Z)
- Convert X(t), Y(t), Z(t) to phi(t), lambda(t), r(t)
- Plot groundtracks
- Print on a text file the results

Formulas can be found in Guidelines.pdf file.

Results:
![](https://i.imgur.com/YcLBrYM.jpg)
![](https://i.imgur.com/Gxx7GKP.jpg)