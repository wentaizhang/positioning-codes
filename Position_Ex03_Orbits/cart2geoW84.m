% ---------------------------------------------------------------
% cart2geoW84 function 
% inputs: vector Cart of GC coordinates -> Cart = [X, Y, Z]
% outputs: vector Geo of G coordinates (phi,lambda in radian, h in meter) -> Geo = [phi, lambda, h]
% ---------------------------------------------------------------
function [Geo]=cart2geoW84(Cart)
% syntax: [Geo]=cart2geoW84(Cart)
% This function convert coordinates from geocentric cartesian (X,Y,Z) to geodetic (phi, lam, h)
%   ellipsoid WGS84
a = 6378137.000000;     %principal semiaxis
f = 1/298.25722356;     %flattening f=(a-b)/a
b=a*(1-f);              %secondary semiaxis 
e1=sqrt(1-b^2/a^2);     %eccentricity
e2=sqrt(a^2/b^2-1);     %secondary eccentricity 
r=sqrt(Cart(:,1).^2+Cart(:,2).^2);
phi=atan2(Cart(:,3),r*sqrt(1-e1^2));   
lam=atan2(Cart(:,2),Cart(:,1));  %longitudine
phai=atan2(Cart(:,3)+e2^2*b*sin(phi).^3,r-e1^2*a*cos(phi).^3);   %latitudine
N=a./sqrt(1-e1^2*sin(phai).^2);
h=r./cos(phai)-N;      %ellipsoidal height
Geo=[phai,lam,h];
end
