% SYNTAX
%  *  [Eta] = ecc_anomaly(M, e)
% OUTPUT
%  *  Eta = eccentricity anomaly
% INPUT
%  *  M = mean anomaly
%  *  e = eccentricity of the orbit
function [Eta] = ecc_anomaly(M, e)
% to start the iterative procedure set Eta equal to M
Eta=M;
max_iter=12;    %   define a max number of iteration
i=1;    %   index of iteration
dEta=1;     % error in Eta estimation between two iteration
thresold = 1e-12;
while (dEta>thresold)&&(i<max_iter)
    Eta_tmp=Eta; 
    Eta=M+e*sin(Eta);
    dEta=Eta-Eta_tmp;
    mod(dEta, 2*pi);    %   return dEta as an angle between (0:2*pi)
    i=i+1;  % increase index of iteration
end
if (i == max_iter)
    fprintf('WARNING: Eccentric anomaly does not converge.\n')
end
