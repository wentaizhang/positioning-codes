% geo2cart function: convert from Local Cartesian to Geocentric Cartesian
% inputs: vector Geo of G coordinates -> Geo = [phi; lambda; h]
% outputs: vector Cart of GC coordinates -> Cart = [X; Y; Z]
function [Cart]= geo2cartW84(Geo)
% ellipsoid WGS84: define a,f,e2,e2b,b as in cart2geoW84
a = 6378137.000000;     %principal semiaxis
f = 1/298.25722356;     %flattening f=(a-b)/a
b=a*(1-f);              %secondary semiaxis
e1_2=1-b^2/a^2;     % square of eccentricity
e2_2=a^2/b^2-1;     % square of secondary eccentricity
% assign the values of latitude, longitude and h in Geo vector to the variables phi_wgs84, lam_wgs84, h_wgs84
phi_wgs84=Geo(1,1);
lam_wgs84=Geo(2,1);
h_wgs84=Geo(3,1);
% compute N as in cart2geoW84
N=a/sqrt(1-e1_2*sin(phi_wgs84)^2);
% compute X_wgs84, Y_wgs84, Z_wgs84 values
X_wgs84=cos(phi_wgs84)*cos(lam_wgs84)*(N+h_wgs84);
Y_wgs84=cos(phi_wgs84)*sin(lam_wgs84)*(N+h_wgs84);
Z_wgs84=(N*(1-e1_2)+h_wgs84)*sin(phi_wgs84);
% assign the X_wgs84, Y_wgs84, Z_wgs84 values to the output vector Cart
Cart=[X_wgs84;Y_wgs84;Z_wgs84];
end