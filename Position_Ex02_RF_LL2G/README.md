## Problem
We have:
1. The Geocentric Cartesian coordinates of A and B in ETRF2000
2. The components of the vertical deflection in A
3. The points C and D have been estimated in a Local Level RF with origin in A and x-axis oriented toward B
4. In the LL, the coordinates of all points A, B, C, D
5. The estimates of the coordinates in Local Level RF have the following accuracies

Compute the Geodetic Coordinates of A, B, C and D in ETRF2000 (sexagesimal angles). Propagate the accuracies given for LL in LC and GC. Propagate also the accuracies in East, North, Up RF with origin in C and in East, North, Up RF with origin in D. 

Note that: 
LL = Local Level
LC = Local Cartesian
GC = Geocentric Cartesian
G = Geodetic
ENU = East, North, Up

## Working Flow

Coordinates
A.	Define the input variables: 
1.	Geocentric Cartesian (GC) coordinates of A and B in ETRF2000,
2.	Components of the vertical deflection in A, 
3.	Local Level (LL) coordinates of A, B, C, D, 
4.	accuracies (covariance matrix) of the LL coordinates 

B.	Create the function ‘rad2sex’ to convert angles from radians to sexadecimals

C.	Create the function ‘cart2geoW84’ to convert coordinates from Geocentric Cartesian to Geodetic

D.	Create the function ‘cart2loca’ to convert coordinates from Geocentric Cartesian to Local Cartesian

E.	Create the function ‘loca2cart’ to convert coordinates from Local Cartesian to Geocentric Cartesian

F.	Create the function ‘geo2cartW84’ to convert from Geodetic to Geocentric Cartesian

G.	Convert A and B from Geocentric Cartesian to Geodetic coordinates

H.	Compute  angle from Local Cartesian coordinates of B
1.	Convert B from Geocentric Cartesian to Local Cartesian coordinates with origin in A
2.	Create rotation matrices
3.	Convert B from Local Cartesian to pseudo Local Level coordinates with origin in A
4.	Use pseudo Local Level coordinates of B to estimate alpha
5.	Compute R rotation matrix
6.	Compute complete rotation matrix from Local Level to Local     Cartesian

I.	Convert C from Local Level to Geodetic coordinates
1.	Convert C from Local Level to Local Cartesian coordinates with origin in A
2.	Convert C from Local Cartesian to Geocentric Cartesian coordinates
3.	Convert C from Geocentric Cartesian to Geodetic coordinates

J.	Convert D from Local Level to Geodetic coordinates
1.	Convert D from Local Level to Local Cartesian coordinates with origin in A
2.	Convert D from Local Cartesian to Geocentric Cartesian coordinates
3.	Convert D from Geocentric Cartesian to Geodetic coordinates

K.	Convert for all points every angle from radians to sexadecimals

L.	Propagate Covariances 

1.	Fill Covariance matrix in LL
2.	Propagate convariances to LC with origin in A (compare with covariances in LL) 
3.	Propagate covariances to GC 
4.	Propagate covariances to LC (East, North, Up) with origin in C (compare with covariances in LC with origin in A) 
5.	Propagate covariances to LC (East, North, Up) with origin in D (compare with covariances in LC with origin in A) 

Formulas in detail can be found in Guidelines.pdf file.

## How to Run
Open Test.m in MATLAB and run it to test the functions.