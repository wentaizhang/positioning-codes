%test rad2sex
alpha=0.7993985036;
[alphad,alpham,alphas]=rad2sex(alpha);

%test cart2geoW84
X_GC=[4398306.554;704149.611;4550154.454];
X_G=cart2geoW84(X_GC);

%test geo2cartW84
X_G=[0.7994070556;0.1587673216;293.287];
X_GC=geo2cartW84(X_G);

%test cart2loca
X0_GC=[4398306.554;704149.611;4550154.454];
dX_GC=[-51.130;76.749;38.681];
[X_LC,R_GC2LC]=cart2loca(X0_GC,dX_GC);

%test loca2cart
X0_GC=[4398306.554;704149.611;4550154.454];
X1_LC=[83.867;54.464;0.993];
[dX_GC,R_LC2GC]=loca2cart(X0_GC,X1_LC);
