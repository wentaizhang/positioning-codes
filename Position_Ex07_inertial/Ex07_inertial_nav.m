% Es Inertial Navigation
% Description: 
% A vehicle is moving in the horizontal plane (x-y inertial system). Two accelerometers and a gyroscope are mounted in the vehicle. 
% The accelerometers measure the acceleration in x and y direction of the system-vehicle; the gyroscope measures the angular velocity wrt 
% the z-axis.
%  y in
%   .   
%   |   . x car
%   |   |   
%   |   |____. y car
%   |
%   |____________. x in
%
% Input data:text file 'inputdata_inert.out' containing
%       ** epoch  = observation epoch's
%       ** ax_car = acceleration in the x direction (in the system vehicle) in m/s2
%       ** ay_car = acceleration in the y direction (in the system vehicle) in m/s2
%       ** omegaz = angular velocity in x-y plane (in the system vehicle) in rad/s
% * alpha0 = asset angle of the vehicle at first epoch
% * vx0, vy0 = velocity of the vechicle in the x and y direction at first epoch (in the system vehicle)
% * x0, y0 = position of the vehicle in the x-y plane  at first epoch (in the inertial system)
% Exercise:Compute the trajectory of the vehicle and plot it.
filename = 'inputdata_inert.out'; % import data with 'importdata' function and create the input
delimiterIn = ' ';
headerlinesIn = 1;
A = importdata(filename,delimiterIn,headerlinesIn);
vx0 = 0; % velocities at first epoch
vy0 = 0;
x0 = 100; % position at first epoch
y0 = 100;
alpha0 = 0; % asset angle at first epoch
dt=1;
t=cat(1,A.data(:,1));
ax_car=cat(1,A.data(:,2));
ay_car=cat(1,A.data(:,3));
omegaz=cat(1,A.data(:,4));
n=length(t);
vx=zeros(n,1);
vy=zeros(n,1);
x_car=zeros(n,1);
x_car(1)=x0;
y_car=zeros(n,1);
y_car(1)=y0;
Rx_car=zeros(n,1);
Ry_car=zeros(n,1);
alpha=zeros(n,1);
vx(1)=vx0;
vy(1)=vy0;
Rx_car(1)=x0;
Ry_car(1)=y0;
alpha(1)=alpha0;
for i=2:n
    vx(i)=vx(i-1)+ax_car(i)*dt; % compute velocities starting from accelerations (equivalent to discrete integration)
    vy(i)=vy(i-1)+ay_car(i)*dt;
    x_car(i)=x_car(i-1)+vx(i-1)*dt+1/2*ax_car(i)*dt^2; % compute positions starting from velocities (equivalent to discrete integration)
    y_car(i)=y_car(i-1)+vy(i-1)*dt+1/2*ay_car(i)*dt^2;
    alpha(i)=alpha(i-1)+omegaz(i)*dt; % compute asset angle in degrees starting from angular velocities (equivalent to discrete integration)
end
% without considering the asset angle (alpha always 0) in the inertial system is valid this relation (look the representation of the two systems)
x_in = y_car;
y_in = x_car;
dx=x_in(2:n)-x_in(1:n-1); % compute dx and dy in the inertial system
dy=y_in(2:n)-y_in(1:n-1);
Rdx=cos(alpha(1:n-1)).*dx+sin(alpha(1:n-1)).*dy; % rotate dx and dy considering the asset angle
Rdy=-sin(alpha(1:n-1)).*dx+cos(alpha(1:n-1)).*dy;
for i=1:n-1
    Rx_car(i+1)=Rx_car(i)+Rdx(i);% compute the rotated coordinate to obtain real trajectory
    Ry_car(i+1)=Ry_car(i)+Rdy(i);
end
% plot the trajectory before and after the rotation and the velocity
figure % figure 1
plot(x_in,y_in,'b.')
hold on
plot(Rx_car,Ry_car,'r.')
hold off
axis equal;
ylim([0,2500]);
xlabel('x of inertial system [m]');
ylabel('y of inertial system [m]');
legend('car trajectory before the rotation','car trajectory after the rotation');
title('Car trajectory in the x-y plane');
figure % figure 2
plot(t,sqrt(vx.^2+vy.^2))
xlabel('epoch [s]');
ylabel('Car velocity [m/s]');
ylim([-10,30])


