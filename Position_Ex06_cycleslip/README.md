## DD analysis and cycle slip identification and repairing

1. Import data in Matlab and graph observed DDs (use function 'importdata')
2. Compute epoch by epoch differences between observed DDs and approximated DDs (residual DDs) and graph them
3. Compute differences between consecutive epochs of residual DDs and graph them (try to use function 'diff' instead of a for cycle)
4. Identify cycle slips and repair them. Use 19 cm for wavelength and 0.20 cycle (3.8 cm) as threshold.
5. 5)	Graph the corrected DDs and to check if all cycle slips have been repaired repeat step 2 and 3

Formulas can be found in Guidelines.pdf file.

Results:
![](https://i.imgur.com/lNTbKRP.jpg)
![](https://i.imgur.com/Q0BWoby.jpg)
![](https://i.imgur.com/rHI22wK.jpg)
![](https://i.imgur.com/6I2L3aS.jpg)
![](https://i.imgur.com/v9pIyrY.jpg)
![](https://i.imgur.com/T2qwHPV.jpg)