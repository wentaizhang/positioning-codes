% Ex06: DD analysis and cycle slip identification and repairing
% Input data = 'DDtSimula.out' text file with
%              col1 = epoch [s]
%              col2 = observed DD [m]
%              col3 = approx DD [m]
% 1) Import data in Matlab and graph observed DDs
[newdata]= importdata('DDtSimula.out');
idata=newdata.data;
% 2) Compute epoch by epoch differences between observed DDs and approximated DDs (residual DDs) and graph them
obs_DD=idata(:,2);
appr_DD=idata(:,3);
epoch=idata(:,1);
residual_DDs=obs_DD-appr_DD;
figure % figure 1
plot(epoch,obs_DD,'b')
xlabel('epoch [s]');
ylabel('Observed DDs [m]');
figure % figure 2
plot(epoch,residual_DDs,'r')
xlabel('epoch [s]');
ylabel('Observed DDs - Approx DDs [m]');
% 3) Compute differences between consecutive epochs of residual DDs and graph them
delta_residual_DDs=diff(residual_DDs);
figure % figure 3
plot(epoch(2:end),delta_residual_DDs,'g')
xlabel('epoch [s]');
ylabel('(ObsDDs(t)-ApxDDs(t))-(ObsDDs(t-1)-ApxDDs(t-1)) [m]');
ylim([-0.2 1]);
% 4) Identify cycle slips and repair them (for cycle)
threshold=0.038;
k=find(abs(delta_residual_DDs)>threshold);
lamda=0.19;
x=delta_residual_DDs(k)/lamda;
n=round(x);
correct_obs_DD=obs_DD;
correct_obs_DD(k+1:end)=correct_obs_DD(k+1:end)-lamda*n;
% 5) Graph the corrected DDs and to check if all cycle slips have been repaired repeat step 2) and 3)
figure % figure 4
plot(epoch,correct_obs_DD,'b')
xlabel('epoch [s]');
ylabel('Corrected DDs [m]');
figure % figure 5
correct_residual_DDs=correct_obs_DD-appr_DD;
plot(epoch,correct_residual_DDs,'r')
xlabel('epoch [s]');
ylabel('Corrected DDs - Approx DDs [m]');
delta_correct_residual_DDs=diff(correct_residual_DDs);
figure % figure 6
plot(epoch(2:end),delta_correct_residual_DDs,'g')
xlabel('epoch [s]');
ylabel('(CorrDDs(t)-ApxDDs(t))-(CorrDDs(t-1)-ApxDDs(t-1)) [m]');
ylim([-0.2 1]);
