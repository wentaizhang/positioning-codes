G1_S1 = csvread('G1-S1.csv'); % Load A-->B data of group 1
G2_S1 = csvread('G2-S1.csv'); 
G3_S1 = csvread('G3-S1.csv'); 
G4_S1 = csvread('G4-S1.csv'); 
G1_S2 = csvread('G1-S2.csv'); % Load B-->A data of group 1
G2_S2 = csvread('G2-S2.csv'); 
G3_S2 = csvread('G3-S2.csv'); 
G4_S2 = csvread('G4-S2.csv'); 
RTK=[G1_S1;G1_S2;G2_S1;G2_S2;G3_S1;G3_S2;G4_S1;G4_S2]; % merge data of all groups
RTK(RTK(:,5)>=0.05,:)=[]; % remove low quality data
iphone_A2B=csvread('A_to_B(wentai).csv');
iphone_B2A=csvread('B_to_A(wentai).csv');
t_iph=[iphone_A2B(:,1);iphone_A2B(end,1)+2+iphone_B2A(:,1)]; % translate the start epoch of B2A to the epoch after the end epoch of A2B
iphone=[iphone_A2B;iphone_B2A]; % merge the A2B and B2A into one data set
[x_iph,y_iph,~] = deg2utm(iphone(:,3),iphone(:,2));
[~,~,xt]=Kalman(t_iph,[x_iph,y_iph],10,0.2);
figure % figure 1
plot(RTK(:,2),RTK(:,3),'r.')
hold on;
plot(x_iph,y_iph,'g.')
plot(xt(:,1),xt(:,2),'b.')
hold off;
axis equal;
xlim([517610,517780]);
xlabel('East-West [m]');
ylabel('North-South [m]');
legend('RTK','iphone raw data','iphone filtered data');
set(gca,'xTickLabel',num2str(get(gca,'xTick')','%d'))
set(gca,'yTickLabel',num2str(get(gca,'yTick')','%d'))
n1=length(iphone);
n2=length(RTK);
distance=zeros(n1,n2);
horizontal_errorsi=zeros(n1,1);% horizontal error of iphone
for i=1:n1
    for j=1:n2
        tmp=[x_iph(i),y_iph(i)]-RTK(j,2:3);
        distance(i,j)=sqrt(sum(tmp.^2));
    end
end

for i=1:n1
    horizontal_errorsi(i,1)=min(distance(i,:));
end

mean_iphone=mean(horizontal_errorsi);
std_iphone=std(horizontal_errorsi);
min_iphone=min(horizontal_errorsi);
max_iphone=max(horizontal_errorsi);
figure % figure 2
plot(t_iph,horizontal_errorsi,'.-')
xlabel('Time [s]');
ylabel('Horizontal Errors of iPhone [m]');
figure % figure 3
binnum=10;
hist(horizontal_errorsi,binnum)
xlim([0,10]);
ylim([0,160]);
xlabel('Horizontal Error of iPhone [m]');
ylabel('Number');
