% read data of each group
G1_S3 = csvread('G1-S3.csv'); 
G2_S3 = csvread('G2-S3.csv'); 
G3_S3 = csvread('G3-S3.csv'); 
G4_S3 = csvread('G4-S3.csv'); 

% remove low quality data from each group
G1_S3(G1_S3(:,5)>=0.05,:)=[]; 
G2_S3(G2_S3(:,5)>=0.05,:)=[]; 
G3_S3(G3_S3(:,5)>=0.05,:)=[];
G4_S3(G4_S3(:,5)>=0.05,:)=[];

figure % figure 1, plot data of each group
plot(G1_S3(:,2),G1_S3(:,3),'r*')
hold on;
plot(G2_S3(:,2),G2_S3(:,3),'g.')
hold on;
plot(G3_S3(:,2),G3_S3(:,3),'b+')
hold on;
plot(G4_S3(:,2),G4_S3(:,3),'ko')
hold off;
axis equal;
xlim([517520,517560]);
xlabel('East-West [m]');
ylabel('North-South [m]');
set(gca,'xTickLabel',num2str(get(gca,'xTick')','%d'))
set(gca,'yTickLabel',num2str(get(gca,'yTick')','%d'))
legend('group 1','group 2','group 3','group 4');

data=[G1_S3;G2_S3;G3_S3;G4_S3]; % merge all groups data into one data set
n1=length(data(:,1));
n=7; % assume the number of points with same reference point is no more than 7
threshold=0.1; % set distance shreshold to 0.1 meter
index=zeros(n1,n); % create a matrix to store records at different epoches with the same reference point
D = pdist2(data(:,2:3), data(:,2:3),'euclid'); % distance between two points
D=triu(D);
[row,col]=find(D<threshold);
N=length(row);
point=zeros(n1,n1);
for i=1:N
    point(row(i),col(i))=D(row(i),col(i)); % 行号，列号代表点号
end
nonzero_num=sum(point~=0,2); % 统计每行非零元素
for i=1:n1
    [~,coli]=find(point(i,:)~=0);
    if ~isempty(coli)
        for j=2:nonzero_num(i)
            if ~all(point(coli(j),:)==0,2)
                point(coli(j),:)=0;
            end
        end
    end
end

a=sum(point~=0,2); % 统计每行非零元素
b=sum(a~=0,1);

fileID=fopen('averaged_coordinates.txt','w');
fprintf(fileID,'%s\r\n','East-West(m),North-South(m),Elevation(m)');
for i=1:n4
    fprintf(fileID,'%6.3f,%6.3f,%6.3f \r\n',[average_data(i,1),average_data(i,2),average_data(i,3)]);
end
fclose(fileID);
android_fast_data=csvread('walkfast-micha.csv');%load smartphone data
android_slow_data=csvread('walkslow-micha.csv');
walkiphone_w=csvread('iphone-wentai.csv');
n5=length(android_fast_data);
n6=length(android_slow_data);
n7=length(walkiphone_w);
Android_fast=zeros(n5,3);
Android_slow=zeros(n6,3);
iphone=zeros(n7,3);
distancefast=zeros(n5,n4);
distanceslow=zeros(n6,n4);
distanceiphone=zeros(n7,n4);
[Android_fast(:,1),Android_fast(:,2),~] = deg2utm(android_fast_data(:,2),android_fast_data(:,1));%convert lat/lon vectors into UTM coordinates (WGS84)
[Android_slow(:,1),Android_slow(:,2),~] = deg2utm(android_slow_data(:,2),android_slow_data(:,1));
[iphone(:,1),iphone(:,2),~] = deg2utm(walkiphone_w(:,2),walkiphone_w(:,1));
%estimate the errors between the smartphone derived trajectory and the RTK measured points
for i=1:n5
    tmp=(Android_fast(i,1:2)-average_data(:,1:2))';
    distancefast(i,:)=sqrt(sum(tmp.^2));% calculate a smartphone trajectory point with all RTK measured points
end
for i=1:n5
    Android_fast(i,3)=find(distancefast(i,:)==min(distancefast(i,:)));% find the point whose distance with the trajectory point is minimum
end
horizontal_errorsf=zeros(n5,1);
for i=1:n5
    horizontal_errorsf(i)=distancefast(i,Android_fast(i,3));% calculate the minimum distance
end

for i=1:n6
    tmp=(Android_slow(i,1:2)-average_data(:,1:2))';
    distanceslow(i,:)=sqrt(sum(tmp.^2));
end
for i=1:n6
    Android_slow(i,3)=find(distanceslow(i,:)==min(distanceslow(i,:)));
end
horizontal_errorss=zeros(n5,1);
for i=1:n6
    horizontal_errorss(i)=distanceslow(i,Android_slow(i,3));
end

for i=1:n7
    tmp=(iphone(i,1:2)-average_data(:,1:2))';
    distanceiphone(i,:)=sqrt(sum(tmp.^2));
end
for i=1:n7
    iphone(i,3)=find(distanceiphone(i,:)==min(distanceiphone(i,:)));
end
horizontal_errorsi=zeros(n5,1);
for i=1:n7
    horizontal_errorsi(i)=distanceiphone(i,iphone(i,3));
end
mean_Android_fast=mean(horizontal_errorsf);% mean horizontal error
mean_Android_slow=mean(horizontal_errorss);
mean_iOS=mean(horizontal_errorsi);
std_Android_fast=std(horizontal_errorsf); %standard deviation of horizontal error
std_Android_slow=std(horizontal_errorss);
std_iOS=std(horizontal_errorsi);
min_Android_fast=min(horizontal_errorsf); % minimum of horizontal error
min_Android_slow=min(horizontal_errorss);
min_iOS=min(horizontal_errorsi);
max_Android_fast=max(horizontal_errorsf); % maxinum of horizontal error
max_Android_slow=max(horizontal_errorss);
max_iOS=max(horizontal_errorsi);
figure % figure 2: plot the RTK and smartphone trajectories
plot(average_data(:,1),average_data(:,2),'r*')
hold on;
plot(Android_fast(:,1),Android_fast(:,2),'b^')
plot(Android_slow(:,1),Android_slow(:,2),'gx')
plot(iphone(:,1),iphone(:,2),'mp')
hold off;
legend('RTK','Android (walk quickly)','Android (walk slowly)','iOS');
axis equal;
set(gca,'xTickLabel',num2str(get(gca,'xTick')','%d'))
set(gca,'yTickLabel',num2str(get(gca,'yTick')','%d'))  
xlabel('East-West [m]');
ylabel('North-South [m]');
t=86400*(android_fast_data(:,4)-floor(android_fast_data(:,4)));
t=t-t(1);
figure % figure 3: plot the horizontal errors of the smartphone as function of time
plot(t,horizontal_errorsf,'r.-')
hold on;
plot(t,horizontal_errorss,'b.-')
plot(walkiphone_w(:,4),horizontal_errorsi,'g.-')
hold off;
legend('Android (walk quickly)','Android (walk slowly)','iOS')
xlabel('time [s]');
ylabel('horizontal errors [m]');
figure % figure 4: plot histograms of the horizontal errors
subplot(1,3,1)
binnum=10;
hist(horizontal_errorsf,binnum)
xlim([0,10]);
ylim([0,160]);
xlabel('horizontal error of Android phone (walk quickly) [m]');
ylabel('number');
subplot(1,3,2)
hist(horizontal_errorss,binnum)
xlim([0,10]);
ylim([0,160]);
xlabel('horizontal error of Android phone (walk slowly) [m]');
ylabel('number');
subplot(1,3,3)
hist(horizontal_errorsi,binnum)
xlim([0,10]);
ylim([0,160]);
xlabel('horizontal error of iOS phone [m]');
ylabel('number');
