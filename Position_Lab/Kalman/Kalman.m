% Grec Florin Catalin, Politecnico di Milano
% 22th of October, 2015
% Ludovico Biagi, 2017.12.11


function [ K_t, G_t, x_t ] = Kalman(t,y,sigma_vv,sigma_ep)
% Kalman(t,y) returns the filtered trajectory of the input time series of horizontal coordinates
% Adopted model: constant velocity: x(t)=x0+v*(t-t0) <> x(t+1) = x(t) + v0*Dt
% Estimated coordinates against input coordinates are plotted
% Estimated coordinates are written in file Estimates.txt
%
%Input
%	t: [n x 1] vector containing the epochs of observations
%   y: [n x 2] observed East and North at t epohs
%   sigma_vv: standard deviation for input coordinates
%   sigma_ep: standard deviation of the model
%Output
%    x_t: cell with estimated East and North for all epochs
%    G_t: cell with Gain matrices for all epochs
%    K_t: cell with K matrices for all epochs
disp('Kalman filter with model constant velocity\n');
    
    K_t = [];
    G_t = [];
    x_t = [];

if nargin < 4 
   fprintf('The number of inputs is not correct. Make sure 4 inputs were passed to this function.\n');
   return
else
%-------------------------Initialisation----------------------------------------------    
    deltat = t(2)-t(1);
    T = [1 0 deltat 0; 0 1 0 deltat; 0 0 1 0; 0 0 0 1];
    A = [1 0 0 0; 0 1 0 0];
    n=length(t);
%------------------------Parameters----------------------------------------
%    sigma_vv = input('Please insert the standard deviation of the input observations in m:\n');
%    sigma_ep = input('Please insert the standard deviation of the model, in m/s:\n');

    main_de = [ sigma_vv, sigma_vv, sigma_ep, sigma_ep ];
    C_e0 = diag((main_de).^2);
    main_dep = [ sigma_ep^2*deltat^2, sigma_ep^2*deltat^2, sigma_ep, sigma_ep ];
    C_ep = diag((main_dep).^2);  
    C_vv = sigma_vv^2*eye(2,2);

    C_e = cell(n,1);
    C_e{1}= C_e0;
    
    for i=2:n
        C_e{i}=zeros(4,4);
    end

    x_t = cell(n,1);
    x_t{1} = [ y(1,1); y(1,2); 1; 0.5];
    
    for i=2:n-1
        x_t{i}=zeros(4,1);
    end

    K_t = cell(n,1);
    G_t = cell(n,1);
    for i=1:n
        K_t{i}=zeros(4,4);
        G_t{i}=zeros(4,2);
    end
%----------------------Kalman Filter---------------------------------------
    for i=1:n-1
    
        K_t{i+1} = C_ep + T*C_e{i}*T';
        G_t{i+1} = K_t{i+1}*A'*([ C_vv + A*K_t{i+1}*A'])^-1; 
        x_t{i+1} = G_t{i+1}*y(i:i,:)' + (eye(4) - G_t{i+1}*A)*T*x_t{i};  % at time t and to t+1
        C_e{i+1} = ( eye(4) - G_t{i+1} * A ) * K_t{i+1};

    end  
%------------------------Outputs-------------------------------------------
    E_est = zeros(size(t));
    N_est = zeros(size(t));
   
    for i = 1 : n
        E_est(i) = x_t{i,1}(1,1);
        N_est(i) = x_t{i,1}(2,1);
    end
  
    x_t = [E_est N_est];
 
    Label_1 = 'East[m]';
    Label_2 = 'North[m]';
    
    fid = fopen('Estimates.txt','w+t');
        fprintf(fid,'%% %s\t%s\n', Label_1, Label_2);
        fprintf(fid,'%5.2f\t\t%5.2f\n', x_t');
    fclose(fid);

    g = figure(1);
    plot(y(:,1),y(:,2),'-rx')
    hold on
    plot(E_est,N_est,'-bo')
    title('Estimated Trajectory vs Observed Trajectory')
    legend('estimate','observed')
    saveas(g,sprintf('Estimted_vs_Measured_trajectory.jpg'));
  
end  
end
 











