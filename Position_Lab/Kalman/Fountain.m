load LinearModel1.txt
t=LinearModel1(:,1);
% x=LinearModel1(:,2);
% y=LinearModel1(:,3);
% Kalman(t,[x,y],10,0.05);
% t=t_iph;
y=[LinearModel1(:,2),LinearModel1(:,3)];
sigma_vv=100;
sigma_ep=0.05;
%-------------------------Initialisation----------------------------------------------    
    deltat = t(2)-t(1);
    T = [1 0 deltat 0; 0 1 0 deltat; 0 0 1 0; 0 0 0 1];
    A = [1 0 0 0; 0 1 0 0];
    n=length(t);
%------------------------Parameters----------------------------------------
%    sigma_vv = input('Please insert the standard deviation of the input observations in m:\n');
%    sigma_ep = input('Please insert the standard deviation of the model, in m/s:\n');

    main_de = [ sigma_vv, sigma_vv, sigma_ep, sigma_ep ];
    C_e0 = diag((main_de).^2);
    main_dep = [ sigma_ep^2*deltat^2, sigma_ep^2*deltat^2, sigma_ep, sigma_ep ];
    C_ep = diag((main_dep).^2);  
    C_vv = sigma_vv^2*eye(2,2);

    C_e = cell(n,1);
    C_e{1}= C_e0;
    
    for i=2:n
        C_e{i}=zeros(4,4);
    end

    x_t = cell(n,1);
    x_t{1} = [ y(1,1); y(1,2); 1; 0.5];
    
    for i=2:n-1
        x_t{i}=zeros(4,1);
    end

    K_t = cell(n,1);
    G_t = cell(n,1);
    for i=1:n
        K_t{i}=zeros(4,4);
        G_t{i}=zeros(4,2);
    end
%----------------------Kalman Filter---------------------------------------
    for i=1:n-1
    
        K_t{i+1} = C_ep + T*C_e{i}*T';
        G_t{i+1} = K_t{i+1}*A'*([ C_vv + A*K_t{i+1}*A'])^-1; 
        x_t{i+1} = G_t{i+1}*y(i:i,:)' + (eye(4) - G_t{i+1}*A)*T*x_t{i};  % at time t and to t+1
        C_e{i+1} = ( eye(4) - G_t{i+1} * A ) * K_t{i+1};

    end  
%------------------------Outputs-------------------------------------------
    E_est = zeros(size(t));
    N_est = zeros(size(t));
   
    for i = 1 : n
        E_est(i) = x_t{i,1}(1,1);
        N_est(i) = x_t{i,1}(2,1);
    end
  
    x_t = [E_est N_est];
 
    Label_1 = 'East[m]';
    Label_2 = 'North[m]';
    
    fid = fopen('Estimates.txt','w+t');
        fprintf(fid,'%% %s\t%s\n', Label_1, Label_2);
        fprintf(fid,'%5.2f\t\t%5.2f\n', x_t');
    fclose(fid);

    figure
    plot(y(:,1),y(:,2),'-r.')
    hold on
    plot(E_est,N_est,'-b.')
    title('Estimated Trajectory vs Observed Trajectory')
    legend('observed','estimate')
  