% - read RTK data of each group
G1_S3 = csvread('G1-S3.csv'); 
G2_S3 = csvread('G2-S3.csv'); 
G3_S3 = csvread('G3-S3.csv'); 
G4_S3 = csvread('G4-S3.csv'); 

% - remove low quality data from each group
G1_S3(G1_S3(:,5)>=0.05,:)=[];
G2_S3(G2_S3(:,5)>=0.05,:)=[];
G3_S3(G3_S3(:,5)>=0.05,:)=[];
G4_S3(G4_S3(:,5)>=0.05,:)=[];

% - figure 1, plot pre-processed RTK data of each group
figure
subplot(2,2,1)
plot(G1_S3(:,2),G1_S3(:,3),'b.')
axis([517525 517555 5036080 5036105])
axis equal;
xlabel('East-West [m]');
ylabel('North-South [m]');
set(gca,'xTickLabel',num2str(get(gca,'xTick')','%d'))
set(gca,'yTickLabel',num2str(get(gca,'yTick')','%d'))
title('Group 1');
subplot(2,2,2)
plot(G2_S3(:,2),G2_S3(:,3),'b.')
axis([517525 517555 5036080 5036105])
axis equal;
xlabel('East-West [m]');
ylabel('North-South [m]');
set(gca,'xTickLabel',num2str(get(gca,'xTick')','%d'))
set(gca,'yTickLabel',num2str(get(gca,'yTick')','%d'))
title('Group 2');
subplot(2,2,3)
plot(G3_S3(:,2),G3_S3(:,3),'b.')
axis([517525 517555 5036080 5036105])
axis equal;
xlabel('East-West [m]');
ylabel('North-South [m]');
set(gca,'xTickLabel',num2str(get(gca,'xTick')','%d'))
set(gca,'yTickLabel',num2str(get(gca,'yTick')','%d'))
title('Group 3');
subplot(2,2,4)
plot(G4_S3(:,2),G4_S3(:,3),'b.')
axis([517525 517555 5036080 5036105])
axis equal;
xlabel('East-West [m]');
ylabel('North-South [m]');
set(gca,'xTickLabel',num2str(get(gca,'xTick')','%d'))
set(gca,'yTickLabel',num2str(get(gca,'yTick')','%d'))
title('Group 4');

data=[G1_S3;G2_S3;G3_S3;G4_S3]; % merge all groups data into one data set
n1=size(data(:,1),1);
n=n1; % assume all points with same reference point, this is the worst case
threshold=0.1; % set distance shreshold to 0.1 meter
index=zeros(n1,n); % create a matrix to store records at different epoches 
%with the same reference point

% for each point, calculate the distance between this point and the points 
% whose ID is after it, if it is less than threshold, put the ID of the 
% latter into the first non-zero element of this row
for i=1:n1-1
    index(i,1)=i;
    k=2;
    for j=i+1:n1
        if norm(data(i,2:3)-data(j,2:3))<threshold
            if index(i,k)==0
                index(i,k)=j;
            else
                while index(i,k)~=0 && k<n
                    k=k+1;
                end
                index(i,k)=j;
            end
        end
    end
end

n2=sum(index~=0,2); % count the number of nonzero elements in each row

for i=1:n1
    if index(i,2)~=0
        for j=2:n2(i)
            rownum=index(i,j);% find row number which has been counted before
            index(rownum,:)=zeros(1,n); % set the rows into all zeros 
        end
    else
        index(i,:)=zeros(1,n);
        % if there is no other points in the range of
        % 0.1 meter, that means this point is an "outlier"
    end
end
index(all(index==0,2),:) = [];%find all-zero rows in index and remove them
n3=sum(index~=0,2);% the number of records measuring each reference point
n4=size((index),1); % the number of reference points
average_data=zeros(n4,3);

% - calculate the average RTK coordinates for each reference point
for i=1:n4
    for j=1:n3(i)
        average_data(i,:)=average_data(i,:)+data(index(i,j),2:4); 
    end
    average_data(i,:)=average_data(i,:)/n3(i);
end

% -	generate an ASCII file with the final averaged RTK coordinates
fileID=fopen('averaged_coordinates.txt','w');
fprintf(fileID,'%s\r\n','East-West(m),North-South(m),Elevation(m)');
for i=1:n4
    fprintf(fileID,'%6.3f,%6.3f,%6.3f \r\n',...
        [average_data(i,1),average_data(i,2),average_data(i,3)]);
end
fclose(fileID);

%load smartphone data
android_fast_data=csvread('walkfast-micha.csv');
android_slow_data=csvread('walkslow-micha.csv');
walkiphone_w=csvread('iphone-wentai.csv');
n5=size(android_fast_data,1);
n6=size(android_slow_data,1);
n7=size(walkiphone_w,1);
Android_fast=zeros(n5,3);
Android_slow=zeros(n6,3);
iphone=zeros(n7,3);
distancefast=zeros(n5,n4);
distanceslow=zeros(n6,n4);
distanceiphone=zeros(n7,n4);
[Android_fast(:,1),Android_fast(:,2),~] = deg2utm(android_fast_data(:,2),...
    android_fast_data(:,1));%convert lat/lon vectors into UTM coordinates (WGS84)
[Android_slow(:,1),Android_slow(:,2),~] = deg2utm(android_slow_data(:,2),...
    android_slow_data(:,1));
[iphone(:,1),iphone(:,2),~] = deg2utm(walkiphone_w(:,2),walkiphone_w(:,1));

%estimate the errors between the smartphone trajectory and RTK points
horizontal_errorsf=zeros(n5,1);
horizontal_errorss=zeros(n6,1);
horizontal_errorsi=zeros(n7,1);
for i=1:n5
    tmp=(Android_fast(i,1:2)-average_data(:,1:2))';
    % distance of a smartphone point with all RTK measured points
    distancefast(i,:)=sqrt(sum(tmp.^2));
    % find the point whose distance with the trajectory point is minimum
    Android_fast(i,3)=find(distancefast(i,:)==min(distancefast(i,:)));  
    horizontal_errorsf(i)=distancefast(i,Android_fast(i,3));
end
for i=1:n6
    tmp=(Android_slow(i,1:2)-average_data(:,1:2))';
    distanceslow(i,:)=sqrt(sum(tmp.^2));
    Android_slow(i,3)=find(distanceslow(i,:)==min(distanceslow(i,:)));
    horizontal_errorss(i)=distanceslow(i,Android_slow(i,3));    
end
for i=1:n7
    tmp=(iphone(i,1:2)-average_data(:,1:2))';
    distanceiphone(i,:)=sqrt(sum(tmp.^2));
    iphone(i,3)=find(distanceiphone(i,:)==min(distanceiphone(i,:)));
    horizontal_errorsi(i)=distanceiphone(i,iphone(i,3));    
end

% - mean horizontal error
mean_Android_fast=mean(horizontal_errorsf);
mean_Android_slow=mean(horizontal_errorss);
mean_iOS=mean(horizontal_errorsi);

% - standard deviation of horizontal error
std_Android_fast=std(horizontal_errorsf);
std_Android_slow=std(horizontal_errorss);
std_iOS=std(horizontal_errorsi);

% - minimum of horizontal error
min_Android_fast=min(horizontal_errorsf);
min_Android_slow=min(horizontal_errorss);
min_iOS=min(horizontal_errorsi);

% - maxinum of horizontal error
max_Android_fast=max(horizontal_errorsf);
max_Android_slow=max(horizontal_errorss);
max_iOS=max(horizontal_errorsi);

% figure 2: plot the RTK average position
figure
plot(average_data(:,1),average_data(:,2),'r*')
xlim([517525 517555]);
axis equal;
set(gca,'xTickLabel',num2str(get(gca,'xTick')','%d'))
set(gca,'yTickLabel',num2str(get(gca,'yTick')','%d'))  
xlabel('East-West [m]');
ylabel('North-South [m]');

% figure 3: plot Android phone trajectory when walking fast
figure
plot(average_data(:,1),average_data(:,2),'r.')
hold on;
plot(Android_fast(:,1),Android_fast(:,2),'b.')
for i=1:n5
    plot([Android_fast(i,1);average_data(Android_fast(i,3),1)],...
        [Android_fast(i,2);average_data(Android_fast(i,3),2)],'g-')
end
hold off;
legend('RTK points','Android trajectory','horizontal error');
xlim([517525 517560]);
axis equal;
set(gca,'xTickLabel',num2str(get(gca,'xTick')','%d'))
set(gca,'yTickLabel',num2str(get(gca,'yTick')','%d'))  
xlabel('East-West [m]');
ylabel('North-South [m]');

% figure 4: plot Android phone trajectory when walking slowly
figure
plot(average_data(:,1),average_data(:,2),'r.')
hold on;
plot(Android_slow(:,1),Android_slow(:,2),'b.')
for i=1:n6
    plot([Android_slow(i,1);average_data(Android_slow(i,3),1)],...
        [Android_slow(i,2);average_data(Android_slow(i,3),2)],'g-')
end
hold off;
legend('RTK points','Android trajectory','horizontal error');
xlim([517525 517560]);
axis equal;
set(gca,'xTickLabel',num2str(get(gca,'xTick')','%d'))
set(gca,'yTickLabel',num2str(get(gca,'yTick')','%d'))  
xlabel('East-West [m]');
ylabel('North-South [m]');

% figure 5 plot iOS phone trajectory
figure
plot(average_data(:,1),average_data(:,2),'r.')
hold on;
plot(iphone(:,1),iphone(:,2),'b.')
for i=1:n7
    plot([iphone(i,1);average_data(iphone(i,3),1)],...
        [iphone(i,2);average_data(iphone(i,3),2)],'g-')
end
hold off;
legend('RTK points','iOS trajectory','horizontal error');
xlim([517525 517560]);
axis equal;
set(gca,'xTickLabel',num2str(get(gca,'xTick')','%d'))
set(gca,'yTickLabel',num2str(get(gca,'yTick')','%d'))
xlabel('East-West [m]');
ylabel('North-South [m]');

% figure 6: plot the horizontal errors of the smartphone as function of time
t=86400*(android_fast_data(:,4)-floor(android_fast_data(:,4)));
t=t-t(1);
figure
plot(t,horizontal_errorsf,'r.-')
hold on;
t=86400*(android_slow_data(:,4)-floor(android_slow_data(:,4)));
t=t-t(1);
plot(t,horizontal_errorss,'b.-')
plot(walkiphone_w(:,4),horizontal_errorsi,'g.-')
hold off;
legend('Android (walk quickly)','Android (walk slowly)','iOS')
xlabel('time [s]');
ylabel('horizontal errors [m]');

% figure 7: plot histograms of the horizontal errors
figure
subplot(1,3,1)
binnum=10;
hist(horizontal_errorsf,binnum)
xlim([0,10]);
ylim([0,80]);
xlabel('horizontal error of Android phone (walk quickly) [m]');
ylabel('number');
subplot(1,3,2)
hist(horizontal_errorss,binnum)
xlim([0,10]);
ylim([0,80]);
xlabel('horizontal error of Android phone (walk slowly) [m]');
ylabel('number');
subplot(1,3,3)
hist(horizontal_errorsi,binnum)
xlim([0,10]);
ylim([0,80]);
xlabel('horizontal error of iOS phone [m]');
ylabel('number');
