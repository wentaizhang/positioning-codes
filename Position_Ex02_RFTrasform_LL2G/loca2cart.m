%--------------------------------------------------------------------------
% loca2cart function 
% inputs: X0 GC coordinates of the origin of LC reference frame, Loca 
%         vector (LC ( = ENU ) coordinates)
% outputs:  baseline_GC, rotation matrix R_LC2GC
%--------------------------------------------------------------------------
function [ baseline_GC, R_LC2GC ] = loca2cart(X0, X_LC)
% syntax: [ baseline_GC, R_LC2GC ] = loca2cart(X0, X_LC)
% This function converts coordinates from local cartesian (e,n,u) to geocentric cartesian (X, Y, Z)
[Geo]=cart2geoW84(X0);      % convert the GC coordinates of X0 in G (cart2geoW84)

l=Geo(2,1);     % assign to variable l the longitude value
p=Geo(1,1);     % assign to variable  p the latitude value

R_GC2LC=[-sin(l),cos(l),0;
        -sin(p)*cos(l),-sin(p)*sin(l),cos(p);
        cos(p)*cos(l),cos(p)*sin(l),sin(p)];
R_LC2GC=R_GC2LC';       % define the rotation matrix R_LC2GC
baseline_GC=R_LC2GC*X_LC;       % compute baseline_GC by multiplying R_LC2GC and X_LC
end

