format longg;
A_GC=[4398306.554; 704149.611; 4550154.454];%Geocentric Cartesian coordinates of A
B_GC=[4398255.424; 704226.360; 4550193.135];%Geocentric Cartesian coordinates of B
xsi=10.23; % vertical deflection components [sec]
eta=9.5; 
xsi=xsi/3600/180*pi; % vertical deflection components [rad]
eta=eta/3600/180*pi;
A_LL=[0;0;0]; % local level coordinates of A [m]
B_LL=[100.0;0.0;1.0]; % local level coordinates of B [m]
C_LL=[80.0;85.0;2.0]; % local level coordinates of C [m]
D_LL=[-25.0;10.0;-3.0]; % local level coordinates of D [m]
Cov_LL=[(10/100)^2,0,0; 
        0,(5/100)^2,0;
        0,0,(15/100)^2];
A_G=cart2geoW84(A_GC); % Geodetic coordinates of A:phi[rad],lambda[rad],h[m]
B_G=cart2geoW84(B_GC); % Geodetic coordinates of B:phi[rad],lambda[rad],h[m]
[Loca, R_GC2LC]=cart2loca(A_GC,B_GC-A_GC);% Loca:Local Cartesian Coordinates of B [m],R_GC2LC:Rotation Matrix from GC2LC
Rxsi=[1,0,0;
      0,cos(xsi),-sin(xsi);
      0,sin(xsi),cos(xsi)];% xsi rotation matrix
Reta=[cos(eta),0,-sin(eta);
        0,1,0;
        sin(eta),0,cos(eta)];% eta rotation matrix
B_pLL=Reta*Rxsi*Loca; % pseudo local level cartesian coordinates of B [m]
ALPHA=atan(B_pLL(2,1)/B_pLL(1,1)); % alpha [rad]
[alpha_deg,alpha_min,alpha_sec]=rad2sex(ALPHA);
Ralp=[cos(ALPHA),sin(ALPHA),0;
          -sin(ALPHA),cos(ALPHA),0;
          0,0,1];% alpha rotation matrix
Rll2lc=(Ralp*Reta*Rxsi)';%complete rotation matrix from LL2CL
C_LC=Rll2lc*C_LL;%local cartesian coordinates of C [m]
[dxAC_GC,R_LC2GC]=loca2cart(A_GC, C_LC);
C_GC=A_GC+dxAC_GC;%geocentric cartesian coordinates of C [m]
[C_G]=cart2geoW84(C_GC);%geodetic coordinates of C: phi[rad],lambda[rad],h[m]
D_LC=Rll2lc*D_LL;%local cartesian coordinates of D: [m]
[dxAD_GC,R_LC2GC]=loca2cart(A_GC, D_LC);
D_GC=A_GC+dxAD_GC;%geocentric cartesian coordinates of D: [m]
[D_G]=cart2geoW84(D_GC);%geodetic coordinates of D: phi[rad],lambda[rad],h[m]
[A_phi_deg,A_phi_min,A_phi_sec]=rad2sex(A_G(1,1));
[A_lambda_deg,A_lambda_min,A_lambda_sec]=rad2sex(A_G(2,1));
[B_phi_deg,B_phi_min,B_phi_sec]=rad2sex(B_G(1,1));
[B_lambda_deg,B_lambda_min,B_lambda_sec]=rad2sex(B_G(2,1));
[C_phi_deg,C_phi_min,C_phi_sec]=rad2sex(C_G(1,1));
[C_lambda_deg,C_lambda_min,C_lambda_sec]=rad2sex(C_G(2,1));
[D_phi_deg,D_phi_min,D_phi_sec]=rad2sex(D_G(1,1));
[D_lambda_deg,D_lambda_min,D_lambda_sec]=rad2sex(D_G(2,1));% geodetic coordinates of A B C D
fprintf('Points =\nA: phi (%g, %g, %g), lambda (%g, %g, %g), %g\nB: phi (%g, %g, %g), lambda (%g, %g, %g), %g\nC: phi (%g, %g, %g), lambda (%g, %g, %g), %g\nD: phi (%g, %g, %g), lambda (%g, %g, %g), %g\n',...
    A_phi_deg,A_phi_min,A_phi_sec,A_lambda_deg,A_lambda_min,A_lambda_sec,A_G(3,1),...
    B_phi_deg,B_phi_min,B_phi_sec,B_lambda_deg,B_lambda_min,B_lambda_sec,B_G(3,1),...
    C_phi_deg,C_phi_min,C_phi_sec,C_lambda_deg,C_lambda_min,C_lambda_sec,C_G(3,1),...
    D_phi_deg,D_phi_min,D_phi_sec,D_lambda_deg,D_lambda_min,D_lambda_sec,D_G(3,1));
Standard_deviation_LL=sqrt(diag(Cov_LL));%LL standard deviations [m]
Cov_LC_A=Rll2lc*Cov_LL*Rll2lc';
standard_deviation_LC_A=sqrt(diag(Cov_LC_A));%LC standard deviations [m]
Cov_GC_A=R_LC2GC*Cov_LC_A*R_LC2GC'; 
standard_deviation_GC_A=sqrt(diag(Cov_GC_A));%GC standard deviations [m]
[~, R_GC2LC_C]=cart2loca(C_GC,zeros(3,1));
Cov_LC_C=R_GC2LC_C*Cov_GC_A*R_GC2LC_C';
standard_deviation_LC_C=sqrt(diag(Cov_LC_C));%standard deviations of C in ENU (with origin in C) [m]
[~, R_GC2LC_D]=cart2loca(D_GC,zeros(3,1));
Cov_LC_D=R_GC2LC_D*Cov_GC_A*R_GC2LC_D';
standard_deviation_LC_D=sqrt(diag(Cov_LC_D));%standard deviations of D in ENU (with origin in D) [m]
