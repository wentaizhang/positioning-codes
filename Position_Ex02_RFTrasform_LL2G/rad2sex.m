% -------------------------------------------
% rad2sex function 
% inputs: angle in radiants
% outputs: angle in degree, minute, second
% -------------------------------------------
function [alpha_deg, alpha_min, alpha_sec] = rad2sex(alpha_rad)
% syntax:[alpha_deg, alpha_min, alpha_sec] = rad2sex(alpha_rad)
% This function convert angles from radians to sexagesimal (degree, minutes, seconds)
a=alpha_rad/pi*180;
alpha_deg=floor(a);
delta_a=a-alpha_deg;
alpha_min=floor(delta_a*60);
delta_a=delta_a*60-alpha_min;
alpha_sec=delta_a*60;
end

