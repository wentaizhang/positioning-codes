%--------------------------------------------------------------------------
% cart2loca function 
% inputs: X0 GC coordinates of the origin of LC reference frame, DX baseline 
% outputs: Loca vector (LC (=ENU) coordinates), rotation matrix R_GC2LC
%--------------------------------------------------------------------------
function [ Loca, R_GC2LC ] = cart2loca(X0, DX)
% syntax: [ Loca, R_GC2LC ] = cart2loca(X0, DX)
% This function convert from geocentric cartesian (X, Y, Z) to local cartesian (e,n,u) with origin in X0
Geo=cart2geoW84(X0);    % compute the G coordinates of X0 (cart2geoW84)

l=Geo(2,1);     % assign to variable l the longitude value
p=Geo(1,1);     % assign to variable  p the latitude value

R_GC2LC=[-sin(l),cos(l),0;      % define the rotation matrix R_GC2LC
        -sin(p)*cos(l),-sin(p)*sin(l),cos(p);
        cos(p)*cos(l),cos(p)*sin(l),sin(p)];
Loca=R_GC2LC*DX;    % compute the ENU coordinates and assign the values to the vector Loca 
end

